import json
import os
import azure

from azure.common.client_factory import get_client_from_cli_profile


from azure.mgmt.resource import ResourceManagementClient

from azure.mgmt.containerinstance import ContainerInstanceManagementClient

from azure.mgmt.containerinstance.models import ContainerPort
from azure.mgmt.containerinstance.models import ResourceRequirements
from azure.mgmt.containerinstance.models import ResourceRequests
from azure.mgmt.containerinstance.models import ResourceLimits
from azure.mgmt.containerinstance.models import Container
from azure.mgmt.containerinstance.models import ImageRegistryCredential
from azure.mgmt.containerinstance.models import IpAddress
from azure.mgmt.containerinstance.models import ContainerGroup
from azure.mgmt.containerinstance.models import Port
from azure.mgmt.containerinstance.models import ContainerGroupIdentity
from azure.mgmt.containerinstance.models import ContainerGroupIdentityUserAssignedIdentitiesValue
from azure.common.credentials import ServicePrincipalCredentials



class AzureSDK:
    def __init__(self, creds, project_name, subscription, image_id, location):
        
        self.aci_client = get_client_from_cli_profile(ContainerInstanceManagementClient, subscription_id=subscription)
        
        self.client = get_client_from_cli_profile(ResourceManagementClient, subscription_id=subscription)
        
        self.location = location

        self.container_group_name =  "container-group-" + project_name
        self.container_name = "container-" + project_name
        
        self.resource_group_name = "rg_" + project_name

        self.os_type = "Linux"
        self.fqdn = project_name
        self.ip_type = "Public"

        self.acr_server = creds["server"]
        # "azurecontainerregisterytest.azurecr.io"
        self.acr_username = creds["username"]
        # "azurecontainerregisterytest"
        self.acr_password = creds["password"]
        # "WSYtOF1mORuVNACfbF6xkvlJQtu6z=7/"
        self.image = self.acr_server + "/repo:" + image_id


    def create_aci(self):
        
        self.create_resource_group()
        
        resources = self.create_resource_requests(2, 2, None)
        ports = [self.create_container_port(80, "TCP"), self.create_container_port(443, "TCP"), self.create_container_port(22, "TCP"), self.create_container_port(5000, "TCP"), self.create_container_port(8080, "TCP")]
        containers = self.create_container(self.container_name, self.image, resources, None, ports, None, None, None, None)

        principal_id ="de44c263-86a4-4a77-b460-f3cf6d505cc6"
        client_id ="82821af7-9f4a-4935-9bbf-b276fa302afa"
        #user_identities = self.get_user_assigned_identities_group_containers(principal_id, client_id)
        #identity = self.create_identity('SystemAssigned', user_identities)
        identity = None

        image_registry_credentials = self.set_registery_credentials(self.acr_server, self.acr_username, self.acr_password)
        container_group_ports = [self.set_container_group_port(80, "TCP"), self.set_container_group_port(443, "TCP"), self.set_container_group_port(22, "TCP"), self.set_container_group_port(5000, "TCP"), self.set_container_group_port(8080, "TCP")]
        ip_address = self.set_container_group_network(container_group_ports, None)
        container_group = self.create_container_group([containers], None, identity, [image_registry_credentials], None, ip_address)

        return self.aci_client.container_groups.create_or_update(self.resource_group_name, self.container_group_name, container_group, custom_headers=None, raw=False, polling=True)

    def create_resource_group(self):
        return self.client.resource_groups.create_or_update(self.resource_group_name, {'location': self.location})

    def create_container_group(self, containers, tags, identity, image_registry_credentials, restart_policy, ip_address):
        return ContainerGroup(containers=containers, os_type=self.os_type, location=self.location, tags=tags, identity=identity, image_registry_credentials=image_registry_credentials, restart_policy=restart_policy, ip_address=ip_address, volumes=None, diagnostics=None, network_profile=None, dns_config=None)

    def set_registery_credentials(self, server, username, password):
        return ImageRegistryCredential(server=server, username=username, password=password)

    def create_container(self, name, image, resources, command, ports, environment_variables, volume_mounts, liveness_probe, readiness_probe):
        return Container(name=name, image=image, resources=resources, command=command, ports=ports, environment_variables=environment_variables, volume_mounts=volume_mounts, liveness_probe=liveness_probe, readiness_probe=readiness_probe)

    def create_resource_requests(self, memory_in_gb, cpu, gpu):
        resource_requests = ResourceRequests(memory_in_gb=memory_in_gb, cpu=cpu, gpu=gpu)
        resource_limits = ResourceLimits(memory_in_gb=memory_in_gb, cpu=cpu, gpu=gpu)
        return ResourceRequirements(requests=resource_requests, limits=resource_limits)

    def create_container_port(self, port, protocol):
        return ContainerPort(port=port, protocol=protocol)

    def set_container_group_port(self, port, protocol):
        return Port(port=port, protocol=protocol)

    def set_container_group_network(self, ports, ip):
        return IpAddress(ports=ports, type=self.ip_type, ip=ip, dns_name_label=self.fqdn)

    #def get_user_assigned_identities_group_containers(self, principal_id, client_id):
    #    return ContainerGroupIdentityUserAssignedIdentitiesValue(principal_id=principal_id, client_id=client_id)

    #def create_identity(self, type, user_id):
    #    return ContainerGroupIdentity(type=type, user_identities=user_id)

    def list_container_groups(self):
        return self.aci_client.container_groups.list()

    def delete_container_group(self):
        return self.aci_client.container_groups.delete(self.resource_group_name, self.container_group_name)
